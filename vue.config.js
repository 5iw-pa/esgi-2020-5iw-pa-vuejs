module.exports = {
    css: {
      loaderOptions: {
        scss: {
          additionalData: `
            @import "@/scss/variables.scss";
            @import "@/scss/mixins.scss";
            @import "@/scss/functions.scss";
            @import "@/scss/form.scss";
            @import "@/scss/establishments.scss";
            @import "@/scss/detailEstablishments.scss";
            @import "@/scss/profile.scss";
            @import "@/scss/fonts.scss";
            @import "@/scss/noty.scss";
          `
        }
      }
    }
  }