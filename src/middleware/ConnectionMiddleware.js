import { getInfoUser } from "../lib/api";

let routeNotConnected = [
    'Login',
    'Home',
    'Register'
]

export default (to, from, next) => {
    let user = JSON.parse(window.localStorage.getItem('User'));
    if (user !== undefined && user !== null) {
        getInfoUser(user.id).then(res => {
            if (res.code > 400) {
                window.localStorage.clear();
                window.localStorage.setItem('env', 'apiplatform');

                if (routeNotConnected.indexOf(to.name) !== -1) {
                    return next();
                }
                return next({ name: 'Login' });
            } else {
                return next();
            }
        })
    } else {
        if (routeNotConnected.indexOf(to.name) !== -1) {
            return next();
        }
        return next({ name: 'Login' });
    }
};