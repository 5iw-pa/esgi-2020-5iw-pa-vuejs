import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../components/Page/Home.vue";
import HelloWorld from "../components/Page/HelloWorld.vue";
import Login from "../components/Page/Login.vue";
import Establishments from "../components/Page/Establishments.vue";
import Register from "../components/Page/Register.vue";
import DetailEstablishment from "../components/Page/DetailEstablishment.vue";
import MyProfile from "../components/Page/MyProfile.vue";
import ModifDetailEstablishment from "../components/Page/ModifDetailEstablishment.vue";
import MyEstablishments from "../components/Page/MyEstablishments.vue";
import ListEstablishments from "../components/Page/ListEstablishments.vue";
import EditHours from "../components/Page/EditHours.vue";
import EditManagers from "../components/Page/EditManagers.vue";
import EditServices from "../components/Page/EditServices.vue";
import Services from "../components/Page/Services.vue";
import ConnectionMiddleware from "../middleware/ConnectionMiddleware";
import CreateEstablishment from "../components/Page/CreateEstablishment.vue";
import ValidNewEstablishments from "../components/Page/ValidNewEstablishments.vue";
import DemandeRattachements from "../components/Page/DemandeRattachements.vue";
import NewAskForJoining from "../components/Page/NewAskForJoining.vue";
import Administration from "../components/Page/Administration.vue";
import ListUsers from "../components/Page/ListUsers.vue";
import PriseRdv from "../components/Page/PriseRdv.vue";
import MyRdv from "../components/Page/MyRdv.vue";

import VueNoty from "vuejs-noty";

Vue.use(VueRouter); 
Vue.use(VueNoty);

const routes = [
    {
        path: "/",
        name: "Home", 
        component: Home,
        props: true
    },
    {
        path: "/hello-world",
        name: "HelloWorld",
        component: HelloWorld,
        props: true
    },
    {
        path: "/establishments",
        name: "Establishments",
        component: Establishments,
        props: true
    },
    {
        path: "/login",
        name: "Login",
        component: Login,
        props: true
    },
    {
        path: "/register",
        name: "Register",
        component: Register,
        props: true
    },
    {
        path: "/my-profile",
        name: "MyProfile",
        component: MyProfile,
        props: true
    },
    {
        path: "/my-establishments",
        name: "MyEstablishments",
        component: MyEstablishments,
        props: true
    },
    {
        path: "/list-establishments",
        name: "ListEstablishments",
        component: ListEstablishments,
        props: true
    },
    {
        path: "/establishment/:encodedId",
        name: "DetailEstablishment",
        component: DetailEstablishment,
        props: true
    },
    {
        path: "/modif-establishment/:encodedId",
        name: "ModifDetailEstablishment",
        component: ModifDetailEstablishment,
        props: false
    },
    {
        path: "/edit-hours/:encodedId",
        name: "EditHours",
        component: EditHours,
        props: true
    },
    {
        path: "/edit-managers/:encodedId",
        name: "EditManagers",
        component: EditManagers,
        props: true
    },
    {
        path: "/services/:encodedId",
        name: "Services",
        component: Services,
        props: true
    },
    {
        path: "/edit-services/:encodedId",
        name: "EditServices",
        component: EditServices,
        props: true
    },
    {
        path: "/create-establishment",
        name: "CreateEstablishment",
        component: CreateEstablishment,
        props: true
    },
    {
        path: "/valid-new-establishments",
        name: "ValidNewEstablishments",
        component: ValidNewEstablishments,
        props: true
    },
    {
        path: "/demande-rattachements",
        name: "DemandeRattachements",
        component: DemandeRattachements,
        props: true
    },
    {
        path: "/nouvelle-demande-rattachement",
        name: "NewAskForJoining",
        component: NewAskForJoining,
        props: true
    },
    {
        path: "/administration",
        name: "Administration",
        component: Administration,
        props: true
    },
    {
        path: "/administration/list-users",
        name: "ListUsers",
        component: ListUsers,
        props: true
    },
    {
        path: "/prise-rdv/:encodedId",
        name: "PriseRdv",
        component: PriseRdv,
        props: true
    }, 
    {
        path: "/mes-rendez-vous",
        name: "MyRdv",
        component: MyRdv,
        props: true
    }
];



const router = new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes
});

router.beforeEach(ConnectionMiddleware);

export default router;