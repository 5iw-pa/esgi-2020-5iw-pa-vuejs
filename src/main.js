import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './assets/styles/index.css';

import VueNoty from 'vuejs-noty'
import 'vuejs-noty/dist/vuejs-noty.css'

import './scss/reset.scss';

Vue.config.productionTip = false

new Vue({
  router,
  VueNoty,
  render: h => h(App),
}).$mount('#app')