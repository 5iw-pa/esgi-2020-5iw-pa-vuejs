import config from "../config";

export const login = (values) => {
    return fetch(`${config.urls[window.localStorage.getItem('env')]}authentication_token`, {
        method: "POST",
        body: JSON.stringify(values),
        headers: {
            "content-type": "application/json",
            accept: "application/json",
        }
    }).then(res => res.json());
}

export const getAllDepartments = async () => {
    let results = await fetch(`${config.urls[window.localStorage.getItem('env')]}api/departments?pagination=false`, {
        method: "GET",
        headers: {
            "content-type": "application/ld+json",
            accept: "application/ld+json"
        }
    }).then(res => res.json()).then(res => results = res);

    results = results["hydra:member"].map(elem => {
        let dpt = [];
        dpt["id"] = elem["@id"];
        dpt["label"] = elem["name"] + ' - ' + elem["departmentCode"];
        return dpt;
    });

    return results;
}

export const register = (values) => {
    let type = values.type;
    delete values.type;
    return fetch(`${config.urls[window.localStorage.getItem('env')]}api/users/${type}/register`, {
        method: "POST",
        body: JSON.stringify(values),
        headers: {
            "content-type": "application/json",
            accept: "application/json",
        }
    }).then(res => res.json());

}

export const getAllEstablishments = async () => {
    let results = await fetch(`${config.urls[window.localStorage.getItem('env')]}api/establishments?pagination=false`, {
        method: "GET",
        headers: {
            "content-type": "application/ld+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        }
    }).then(res => res.json()).then(res => results = res);

    results = results["hydra:member"];

    return results;
}

export const getDataEstablishment = async (uri) => {
    let results = await fetch(`${config.urls[window.localStorage.getItem('env')]}${uri.substring(1)}`, {
        method: "GET",
        headers: {
            "content-type": "application/ld+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        }
    })

    return results.json();
}

export const getInfoUser = async (ID) => {
    let results = await fetch(`${config.urls[window.localStorage.getItem('env')]}api/users/${ID}`, {
        method: "GET",
        headers: {
            "content-type": "application/ld+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        }
    })

    return results.json();
}

export const updateUser = async (ID, user) => {
    let results = await fetch(`${config.urls[window.localStorage.getItem('env')]}api/users/${ID}`, {
        method: "PATCH",
        body: JSON.stringify(user),
        headers: {
            "content-type": "application/merge-patch+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        }
    })

    return results.json();
}

export const updateDataEstablishment = async (body, uri) => {
    let results = await fetch(`${config.urls[window.localStorage.getItem('env')]}${uri.substring(1)}`, {
        method: "PATCH",
        body: JSON.stringify(body),
        headers: {
            "content-type": "application/merge-patch+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        }
    })

    return results.json();
}

export const activeEstablishment = async (body) => {
    let results = await fetch(`${config.urls[window.localStorage.getItem('env')]}${body['@id'].substring(1)}`, {
        method: "PATCH",
        body: JSON.stringify(body),
        headers: {
            "content-type": "application/merge-patch+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        }
    })

    return results.json();
}

export const getListEstablishmentsByRole = async(role) => {
    let results;
    await fetch(`${config.urls[window.localStorage.getItem('env')]}api/establishments/${role}/get-my-establishments`, {
        method: "GET",
        headers: {
            "content-type": "application/ld+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        }
    }).then(res => res.json()).then(res => results = res)

    results = results["hydra:member"];
    return results;
}

export const getDays = async() => {
    let results;
    await fetch(`${config.urls[window.localStorage.getItem('env')]}api/days`, {
        method: "GET",
        headers: {
            "content-type": "application/ld+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        }
    }).then(res => res.json()).then(res => results = res)

    let obj = {};
    results["hydra:member"].forEach(elem => {
        obj[elem["name"]] = elem["@id"];
    });
    return obj;
}

export const getShifts = async() => {
    let results;
    await fetch(`${config.urls[window.localStorage.getItem('env')]}api/type_of_shifts`, {
        method: "GET",
        headers: {
            "content-type": "application/ld+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        }
    }).then(res => res.json()).then(res => results = res)

    let obj = {};
    results["hydra:member"].forEach(elem => {
        obj[elem["name"]] = elem["@id"];
    });
    return obj;
}

export const updateHours = async(Day, Shift, uri, value) => {
    let results;
    let body = {
        startHour: value['startHour'],
        endHour: value['endHour'],
        day: Day,
        typeOfShift: Shift,
        establishment: uri,
        nbBarber: 1
    };
    await fetch(`${config.urls[window.localStorage.getItem('env')]}api/hours`, {
        method: "POST",
        headers: {
            "content-type": "application/ld+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        },
        body: JSON.stringify(body)
    }).then(res => res.json()).then(res => results = res)
    
    return results;
}

export const createEstablishment = async(body) => {
    return await fetch(`${config.urls[window.localStorage.getItem('env')]}api/establishments/manager/create-my-establishment`, {
        method: "POST",
        headers: {
            "content-type": "application/ld+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        },
        body: JSON.stringify(body)
    }).then(res => res.json())
}

export const getEstablishmentsInactive = async () => {
    let results;
    await fetch(`${config.urls[window.localStorage.getItem('env')]}api/establishments/get-establishments-inactive`, {
        method: "GET",
        headers: {
            "content-type": "application/ld+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        }
    }).then(res => res.json()).then(res => results = res)
    results = results['hydra:member'];
    return results;
}

export const getPendingRequest = async (id) => {
    let results;
    await fetch(`${config.urls[window.localStorage.getItem('env')]}api/user_establishments/owner/get-pending-requests?establishment=${id}`, {
        method: "GET",
        headers: {
            "content-type": "application/ld+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        }
    }).then(res => res.json()).then(res => results = res)
    results = results['hydra:member'];
    return results;
}

export const askForJoinEstablishment = async (siret) => {
    let results = [];
    let body= {
        siret
    };
    await fetch(`${config.urls[window.localStorage.getItem('env')]}api/user_establishments/manager/ask-for-join-establishment`, {
        method: "POST",
        headers: {
            "content-type": "application/ld+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        },
        body: JSON.stringify(body)
    }).then(res => res.json()).then(res => results = res)

    return results;
}

export const deleteUser = async (id) => {
    await fetch(`${config.urls[window.localStorage.getItem('env')]}api/users/${id}`, {
        method: "DELETE",
        headers: {
            "content-type": "application/ld+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        }
    })
}

export const deleteEstablishment = async (id) => {
    await fetch(`${config.urls[window.localStorage.getItem('env')]}api/establishments/${id}`, {
        method: "DELETE",
        headers: {
            "content-type": "application/ld+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        }
    })

}

export const getUsers = async () => {
    let results;
    await fetch(`${config.urls[window.localStorage.getItem('env')]}api/users?pagination=false`, {
        method: "GET",
        headers: {
            "content-type": "application/ld+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        }
    }).then(res => res.json()).then(res => results = res)
    results = results['hydra:member'];
    return results;
}

export const updateStatusOfManager = async (id, status) => {
    let body = {
        validated: status
    };

    let results = await fetch(`${config.urls[window.localStorage.getItem('env')]}api/user_establishments/${id}`, {
        method: "PATCH",
        body: JSON.stringify(body),
        headers: {
            "content-type": "application/merge-patch+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        }
    })

    return results.json();
}

export const createService = async (body) => {
    let results;
    await fetch(`${config.urls[window.localStorage.getItem('env')]}api/services`, {
        method: "POST",
        headers: {
            "content-type": "application/ld+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        },
        body: JSON.stringify(body)
    }).then(res => res.json()).then(res => results = res)

    return results;
}

export const getService = async (uri) => {
    let results;
    await fetch(`${config.urls[window.localStorage.getItem('env')]}${uri.substring(1)}`, {
        method: "GET",
        headers: {
            "content-type": "application/ld+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        }
    }).then(res => res.json()).then(res => results = res);

    return results;
}

export const updateService = async (id, body) => {

    let results = await fetch(`${config.urls[window.localStorage.getItem('env')]}api/service/${id}`, {
        method: "PATCH",
        body: JSON.stringify(body),
        headers: {
            "content-type": "application/merge-patch+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        }
    })

    return results.json();
}

export const getAppointments = async (id, date) => {
    
    let results;
    await fetch(`${config.urls[window.localStorage.getItem('env')]}api/appointments/get-establishment-appointments-at-date?establishment=${id}&date=${date}`, {
        method: "GET",
        headers: {
            "content-type": "application/ld+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        }
    }).then(res => res.json()).then(res => results = res);

    results = results['hydra:member'];

    return results;
}

export const createAppointment = async (body) =>  {
    return await fetch(`${config.urls[window.localStorage.getItem('env')]}api/appointments`, {
        method: "POST",
        headers: {
            "content-type": "application/ld+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        },
        body: JSON.stringify(body)
    }).then(res => res.json())
}

export const getAllMyAppointments = async () => {
    let results;
    await fetch(`${config.urls[window.localStorage.getItem('env')]}api/appointments/client/get-all-my-appointments`, {
        method: "GET",
        headers: {
            "content-type": "application/ld+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        }
    }).then(res => res.json()).then(res => results = res);

    results = results['hydra:member'];

    return results;
}

export const updateRattachement = async (value, ID) => {
    let body = {
        validated: value
    };

    let results = await fetch(`${config.urls[window.localStorage.getItem('env')]}api/user_establishments/${ID}`, {
        method: "PATCH",
        body: JSON.stringify(body),
        headers: {
            "content-type": "application/merge-patch+json",
            accept: "application/ld+json",
            "Authorization": `Bearer ${window.localStorage.getItem('JWT')}`
        }
    })

    return results.json();
}