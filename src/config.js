export default {
    urls: {
        // apiplatform: "http://localhost:8000/",
        apiplatform: "https://iw5-pa-apiplatform.herokuapp.com/",
        prisma: "http://localhost:prisma"
    }
}